#include "types.h"
#include "stat.h"
#include "user.h"

void create_children(int depth, int child_count, int first){
    if(depth <= 0){
        sleep(25);
        return;
    }
    int parent_pid = getpid();
    for(int i=0;i<child_count;i++){
        int pid = fork();
        sleep(depth * 40 + 20);
        if(pid < 0)
            break;
        if(pid==0){
            printf(1, "parent pid: %d, current pid: %d\n", parent_pid, getpid());
            sleep(25);
            create_children(depth-1, child_count, 0);
            sleep(10000);
            exit();
        }
    }
    if(first != 1){
        while (wait() != -1){}
        exit();
    }
}

int
main(int argc, char *argv[]) {
    printf(1, "proc info :\n");
    create_children(3,3, 1);
    sleep(1000);
    printf(1, "generations :\n");
    print_generation(getpid());
    printf(1, "finish!\n");
    while (wait() != -1) {}
    exit();
}
