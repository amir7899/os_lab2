#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
    if(argc == 1)
        printf(1,"%d\n", creation_time(-1));
    if(argc != 2)
        exit();
    printf(1,"%d\n", creation_time(atoi(argv[1])));
    exit();
}
