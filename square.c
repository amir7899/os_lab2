#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
    if(argc != 2){
        exit();
    }
    printf(1,"%d\n", calculate_biggest_perfect_square(atoi(argv[1])));
    exit();
}
