#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}


int
sys_set_sleep(void){


    int seconds;
    uint ticks0;
    struct proc* p = myproc();
    if(argint(0,&seconds) < 0){
        return -1;
    }
    acquire(&tickslock);
    ticks0 = ticks;
    while (ticks-ticks0 < 100 * seconds)
    {
        if(myproc()->killed){
            release(&tickslock);
            return -1;
        }
        release(&tickslock);
        p->state = SLEEPING;
        p->chan = &ticks;
        sti();
        p->state = RUNNING;
        p->chan = 0;
        acquire(&tickslock);
    }
    release(&tickslock);
    return 0;
}

int
sys_get_time(void){
    struct rtcdate *now;
    argptr(0, (void*)&now, sizeof(*now));
    cmostime(now);
    cprintf("\ntime is : %d %d %d %d %d %d\n", now->second, now->minute, now->hour, now->day, now->month, now->year);
    return (now->second) + (now->minute*60) + (now->hour*3600);
}


int
sys_calculate_biggest_perfect_square(void)
{
    int n;
    if(argint(0, &n) < 0)
        return -1;
    int i= 0;
    while(i*i<=n){
        i++;
    }
    i--;
    return i;
    //return i*i;
}

int
sys_creation_time(void){
    int pid;
    if(argint(0, &pid) < 0)
        return -1;
    if(pid < 0)
        return creation_time(myproc()->pid);
    return creation_time(pid);
}

void
sys_print_generation(void){
    int pid;
    if(argint(0, &pid) < 0)
        return;
    if(pid < 0)
        print_generation(myproc()->pid);
    else
        print_generation(pid);
}


int
sys_get_ancestors(void)
{
    int pid;
    argint(0, &pid);
    cprintf("first pid: %d\n", pid);
    ancestors(pid);
    return 0;
}