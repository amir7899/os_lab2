#include "types.h"
#include "stat.h"
#include "user.h"
#include "date.h"

int main(int argc, char *argv[])
{
    struct rtcdate *now = malloc(sizeof(struct rtcdate));
    int now_second = get_time(now);
    set_sleep(4);
    printf(1, "%d\n", get_time(now) - now_second);
    exit();
}
